from setuptools import setup
from Cython.Build import cythonize
from Cython.Compiler import Options

Options.docstrings = False

setup(
    ext_modules = cythonize("tps.py", compiler_directives={'language_level' : "3"})
)
