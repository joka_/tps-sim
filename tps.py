#!/usr/bin/env python3

import random
import sys
import json
import logging
from numpy import mean
import concurrent.futures
import itertools
from pprint import pprint
from copy import deepcopy
import time
import multiprocessing


class Boss:
  def __init__(self, settings):
    self.speed = float(settings['speed'])
    self.min_damage = int(settings['damage'])
    self.max_damage = int(settings['damage'])
    self.dmg_var = float(settings['damage_var'])
    self.armor = float(settings['armor'])
    self.dr = self.armor / float(self.armor + 400 + 85 * (63 + 4.5 * (63 - 59)))
    self.burst_interval = float(settings['burst_interval'])
    self.burst_damage = int(settings['burst_damage'])
    self.burst_damage_var = float(settings['burst_damage_var'])
    self.burst_is_magic = bool(settings['burst_is_magic'])

    self.reset()

  def reset(self):
    self.swing = 0
    self.burst = self.burst_interval

  def tick(self, delay):
    self.swing -= delay
    self.burst -= delay

  def parry_haste(self):
    remaining = self.swing/self.speed
    if remaining > 0.6:
      self.swing -= 0.4 * self.speed
    elif remaining > 0.2:
      self.swing = self.swing * 0.8

  def attack(self, player):
    if self.swing <= 0:
      if not player.is_off_tank:
        dmg = random.randint(self.min_damage, self.max_damage)
        dmg = dmg * random.uniform(1.0 - self.dmg_var, 1.0 + self.dmg_var)
        player.defend(dmg)
      self.swing = self.speed
    if self.burst <= 0:
      if not player.is_off_tank:
        dmg = self.burst_damage * random.uniform(1.0 - self.burst_damage_var, 1.0 + self.burst_damage_var)
        player.defend(dmg, is_magic=self.burst_is_magic)
      self.burst = self.burst_interval


class Player:
  def __init__(self, p, _set, _equipped, wf = True, debug = False):
    self.wf = wf
    self.start_rage = int(p['start_rage'])
    self.is_off_tank = bool(p['off_tank'])
    self.infinite_rage = bool(p['infinite_rage'])
    self.talents = deepcopy(p['talents'])
    self.sets = deepcopy(p['sets'])

    self.counters = {
      'wh': 0,
      'wf': 0,
      'rev': 0,
      'hs': 0,
      'ss': 0,
      'sa': 0,
      'sb': 0,
      'bt': 0
    }

    # Stats
    self.attack_power = int(p['stats']['attack_power'] + self.talents['improved_battle_shout'])
    self.crit = float(p['stats']['crit'] + self.talents['cruelty'])
    self.hit = float(p['stats']['hit'])
    self.block = float(p['stats']['block'] + self.talents['improved_shield_block'])
    self.block_value = float(p['stats']['block_value'])
    self.parry = float(p['stats']['parry'] + self.talents['deflection'])
    self.dodge = float(p['stats']['dodge'])
    self.defense = int(p['stats']['defense']) + int(self.talents['anticipation'])
    self.armor = int(p['stats']['armor'] * float(1 + self.talents['toughness']))
    self.weapon_skills = deepcopy(p['stats']['weapon_skill'])

    # Main-hand
    self.main_hand = deepcopy(p['main_hand'])

    # Off-hand
    self.off_hand = deepcopy(p['off_hand']) if 'off_hand' in p else None

    # Gear procs
    self.gear_procs = deepcopy(p['gear_procs'])

    _defense = self.defense

    if debug:
      print("Hit: %.4f" % self.hit)
    # Re-equip
    for (i, eq) in zip(_set, _equipped):
      if i != eq:
        self.equip(eq, unequip=True, debug=debug)
        self.equip(i, debug=debug)
    self.hit = round(self.hit, 2)  # Silly python..
    if debug:
      print("Hit after: %.4f\n----------" % self.hit)

    _defense_diff = self.defense - _defense
    if debug:
      print("Defense before: %d, after: %d, diff: %d" % (_defense, self.defense, _defense_diff))

    if abs(_defense_diff) > 0.1:
      if debug:
        print("Defense recalc. BEOFRE - dodge=%.4f, parry=%.4f, block=%.4f" % (self.dodge, self.parry, self.block))
      self.dodge += 0.0004 * _defense_diff
      self.parry += 0.0004 * _defense_diff
      self.block += 0.0004 * _defense_diff
      if debug:
        print("Defense recalc. AFTER - dodge=%.4f, parry=%.4f, block=%.4f" % (self.dodge, self.parry, self.block))

    if self.__has_set("might", 3):
      self.block_value += 30

    if self.__has_set("wrath", 3):
      self.attack_power += 30

    if self.__has_set("zandalar", 2):
      self.block += 0.02

    if self.__has_set("hakkari_blades", 2):
      self.weapon_skills['sword'] += 6

    if "DF" in self.gear_procs:
      self.attack_power += 150
      self.block_value += 4
      self.df_timer = 60.0
    else:
      self.df_timer = 0.0

    self.reset()

    # Buffs
    self.attack_power += 193  # Battle Shout rank 6 (rank 7 in aq = 232)
    if p['buffs']['raid']:
      self.armor += 385  # MotW
      self.armor += 32  # MotW
      self.attack_power += 32  # MotW
      self.crit += 0.008  # MotW
      self.dodge += 0.008  # MotW
      self.block_value += 1  # MotW
      self.armor += 176  # GoA
      self.crit += 0.044  # GoA
      self.dodge += 0.044  # GoA
      self.attack_power += 176  # SoE
      self.block_value += 4  # SoE
    if p['buffs']['consumables']:
      self.armor += 450  # Superior Defense
      self.crit += 0.0325  # Mongoose
      self.armor += 50  # Mongoose
      self.dodge += 0.0125  # Mongoose
      self.attack_power += 50  # Giants
      self.block_value += 1  # Giants
    if p['buffs']['world']:
      self.crit += 0.05  # Ony
      self.attack_power += 140  # Ony
      self.haste_buffs.append((1.15, 9999, 9999))  # Wcb
      self.attack_power += 200  # DM:T
      self.attack_power += 40  # Zandalar (roughly)
      self.block_value += 2  # Zandalar (roughly)
      self.crit += 0.0135  # Zandalar (roughly)
      self.dodge += 0.0135  # Zandalar (roughly)
      self.armor += 54  # Zandalar (roughly)

  def equip(self, item, unequip=False, debug=False):
    c = -1 if unequip else 1

    self.attack_power += (item['attack_power'] * c) if 'attack_power' in item else 0
    self.crit += (item['crit'] * c) if 'crit' in item else 0
    self.hit += (item['hit'] * c) if 'hit' in item else 0 
    self.block += (item['block'] * c) if 'block' in item else 0
    self.block_value += (item['block_value'] * c) if 'block_value' in item else 0
    self.parry += (item['parry'] * c) if 'parry' in item else 0 
    self.dodge += (item['dodge'] * c) if 'dodge' in item else 0
    self.defense += (item['defense'] * c) if 'defense' in item else 0
    self.armor += (item['armor'] * c) if 'armor' in item else 0

    if debug:
      print(("Unequip" if unequip else "Equip") + ": " + item['name'])
      print("Current hit: %.4f" % self.hit)

    if 'set' in item:
      s = item['set']
      if s in self.sets:
        self.sets[s] += c
        if debug:
          print(("Unequip" if unequip else "Equip") + ": " + item['name'])
          print("%s n: %d" % (s, self.sets[s]))
      else:
        self.sets[s] = c
        if debug:
          print(("Unequip" if unequip else "Equip") + ": " + item['name'])
          print("%s n: %d" % (s, self.sets[s]))

    if 'weapon_skill' in item:
      for t in item['weapon_skill']:
        self.weapon_skills[t] += (item['weapon_skill'][t] * c)

    if not unequip:
      if item['slot'] == "main_hand":
        self.main_hand = item
      elif item['slot'] == "off_hand":
        self.off_hand = item

    if 'special' in item:
      if unequip:
        self.gear_procs.remove(item['special'])
      else:
        self.gear_procs.append(item['special'])
      #print("self.gear_procs=",self.gear_procs)

  def reset(self):
    self.rage = self.start_rage
    self.swing = 0
    self.oh_swing = 0 if (self.off_hand is not None and self.off_hand['type'] != "shield") else 9999
    self.total_threat = 0
    self.damage_taken = 0
    self.damage_done = 0
    self.anger_management_tick = 3.0
    self.enraged = 0
    self.warriors_wrath = False

    self.deep_wounds_buff = 0
    self.deep_wounds_tick = 3.0
    self.deep_wounds_dmg = 0

    self.revenge_cd = 0
    self.revenge_active = False

    self.shield_slam_cd = 0 if (self.talents['shield_slam'] and self.off_hand is not None and self.off_hand['type'] == "shield" ) else 9999
    self.bloodthirst_cd = 0 if self.talents['bloodthirst'] else 9999

    self.shield_block_cd = 4 if (self.talents['use_shield_block'] and self.off_hand is not None and self.off_hand['type'] == "shield") else 9999  # Assume it was used 1s before the fight starts
    self.shield_block_stacks = 2 if (self.talents['use_shield_block'] and self.off_hand is not None and self.off_hand['type'] == "shield") else 0

    self.bloodrage_cd = 57  # Assume it was used 3s before the fight starts
    self.bloodrage_buff = 7.0

    self.death_wish_cd = 0
    self.death_wish_buff = 0

    self.haste_buffs = []

  def tick(self, delay):
    self.revenge_cd -= delay
    self.shield_slam_cd -= delay
    self.bloodthirst_cd -= delay
    self.shield_block_cd -= delay
    self.swing -= delay
    self.oh_swing -= delay
    self.anger_management_tick -= delay
    self.death_wish_buff -= delay
    self.enraged -= delay
    self.df_timer -= delay

    if self.death_wish_buff <= 0 and (self.death_wish_buff + delay) > 0:
      #logging.info("  Death Wish faded.")
      pass

    if self.df_timer <= 0 and (self.df_timer + delay) > 0:
      #logging.info("  Diamond Flask faded.")
      self.attack_power -= 150
      self.block_value -= 4

    if self.deep_wounds_buff > 0:
      self.deep_wounds_buff -= delay
      self.deep_wounds_tick -= delay
      if self.deep_wounds_tick <= 0:
        self.deep_wounds_tick = 3.0
        self.total_threat += self.__threat(self.deep_wounds_dmg)
        #logging.info("  Deep Wounds tick %d damage (%d total threat)" % (self.deep_wounds_dmg, self.total_threat))

    buffs = []
    for (buff, duration, stacks) in self.haste_buffs:
      buffs.append((buff, duration - delay, stacks))
    self.haste_buffs = buffs

    if self.bloodrage_buff >= 0:
      prev = self.bloodrage_buff
      self.bloodrage_buff -= delay
      if int(prev) > int(self.bloodrage_buff):
        #logging.info("  Bloodrage tick.")
        self.__gain_rage(1)
      if self.bloodrage_buff <= 0:
        #logging.info("  Bloodrage faded.")
        pass

    if self.anger_management_tick <= 0:
      self.anger_management_tick = 3.0
      if self.talents['anger_management']:
        self.__gain_rage(1)
        #logging.info("  Anger Management tick.")

    if self.infinite_rage:
      self.rage = 100

  def __current_speed(self, off_hand=False):
    w = self.off_hand if off_hand else self.main_hand
    result = w['speed']

    keep = []
    for (buff, duration, stacks) in self.haste_buffs:
      if duration > 0 and stacks > 0:
        result /= buff
        keep.append((buff, duration, stacks))
    self.haste_buffs = keep

    return result

  def __remove_buff_stacks(self):
    buffs = []
    for (buff, duration, stacks) in self.haste_buffs:
      buffs.append((buff, duration, stacks - 1))
    self.haste_buffs = buffs

  def white_hit(self, boss: Boss, off_hand=False):
    dmg = self.__weapon_damage(off_hand=off_hand)
    dmg, crit = self.__attack(dmg, 0, boss, white=True, off_hand=off_hand)
    self.__gain_rage(self.__rage(dmg, crit))
    if dmg > 0:
      self.counters['wh'] += 1
      self.on_hit_procs(boss, crit, off_hand=off_hand)

    if off_hand:
      self.oh_swing = self.__current_speed(off_hand=True)
    else:
      self.swing = self.__current_speed(off_hand=False)

    self.total_threat += self.__threat(dmg)
    #logging.info("  Swing for %d damage%s%s. Total threat: %d" % (dmg, " (critical)" if crit else "", " with off-hand" if off_hand else "", self.total_threat))

  def on_hit_procs(self, boss: Boss, was_crit, was_wf=False, off_hand=False, was_hoj=False):
    self.__remove_buff_stacks()
    self.__weapon_proc(boss, off_hand=off_hand)
    if was_crit:
      self.flurry()
      self.deep_wounds(off_hand=off_hand)

    if not was_hoj and "HoJ" in self.gear_procs and random.random() <= 0.02:
      dmg = self.__weapon_damage()
      dmg, crit = self.__attack(dmg, 0, boss, white=True)
      if dmg > 0:
        self.on_hit_procs(boss, crit, was_hoj=True)
      self.total_threat += self.__threat(dmg)
      #logging.info("  HoJ proc for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      self.__gain_rage(self.__rage(dmg, crit))

    # Windfury
    if not was_wf and not off_hand and self.wf and random.random() <= 0.2:
      self.counters['wf'] += 1
      dmg = self.__weapon_damage(bonus_ap=315)
      dmg, crit = self.__attack(dmg, 0, boss, white=True)
      if dmg > 0:
        self.on_hit_procs(boss, crit, was_wf=True)
      self.total_threat += self.__threat(dmg)
      #logging.info("  Windfury for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      self.__gain_rage(self.__rage(dmg, crit))

  def __weapon_proc(self, boss: Boss, off_hand=False):
    w = self.off_hand if off_hand else self.main_hand
    wp = w['proc'] if 'proc' in w else None

    if not off_hand and wp and random.random() <= wp['rate']:
      #logging.debug("  %s procced!" % w['name'])
      if 'min_damage' in wp and 'max_damage' in wp:
        dmg = random.randint(wp['min_damage'], wp['max_damage'])
        # TODO: "attack" boss with proc
        if wp['armor']:
          dmg = int(dmg * (1.0 - boss.dr))
        self.total_threat += self.__threat(dmg, wp['extra_threat'] if 'extra_threat' in wp else 0)
        #logging.info("  Weapon proc for %d damage. Total threat: %d" % (dmg, self.total_threat))
      if 'haste' in wp:
        self.haste_buffs.append((wp['haste'], wp['duration'], wp['stacks']))

  def enrage(self):
    if self.talents['enrage'] > 0.0:
      #logging.info("  Enrage!")
      self.enraged = 12   # TODO: max swings = 12

  def flurry(self):
    if self.talents['flurry'] > 0.0:
      #logging.info("  Flurry!")
      self.haste_buffs.append((1.0 + self.talents['flurry'], 30, 3))  # Not sure about duration

  def deep_wounds(self, off_hand=False):
    #logging.info("   Deep Wounds disabled")

    #if self.talents['deep_wounds'] > 0.0:
    #  #logging.info("  Deep Wounds applied")
    #  w = self.off_hand if off_hand else self.main_hand
    #  self.deep_wounds_dmg = int(((w['min_damage'] + w['max_damage']) / 2.0) / 4.0)  # 4 ticks total (if 3s between each)
    #  self.deep_wounds_dmg *= self.talents['deep_wounds']
    #  self.deep_wounds_buff = 12
    pass

  def shield_block(self):
    if self.shield_block_cd <= 0 and self.rage >= 10:
      self.counters['sb'] += 1
      self.rage -= 10  # Warrior's Wrath does not apply here
      self.shield_block_cd = 5
      self.shield_block_stacks = 2
      #logging.info("  Shield block used. Current rage: %d" % self.rage)

  def bloodrage(self):
    if self.bloodrage_cd <= 0 and not self.infinite_rage:
      self.bloodrage_cd = 60
      self.bloodrage_buff = 10.0
      #logging.info("  Bloodrage used.")
      self.__gain_rage(10 + self.talents['improved_bloodrage'])
      return True
      
    return False

  def death_wish(self):
    if self.talents['death_wish'] and self.death_wish_cd <= 0:
      self.death_wish_cd = 180
      self.death_wish_buff = 30
      #logging.info("  Death Wish used.")
      return True

    return False

  def shield_slam(self, boss: Boss):
    if self.talents['shield_slam'] and self.shield_slam_cd <= 0 and self.has_rage(20, False):
      self.shield_slam_cd = 6
      dmg = random.randint(342, 358) + self.block_value
      dmg, crit = self.__attack(dmg, 20, boss)
      if dmg > 0:
        self.counters['ss'] += 1
        self.total_threat += self.__threat(dmg, 250)
        #self.on_hit_procs(boss)  # shield slam never triggers wf
      #logging.info("  Shield Slam for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      return True

    return False

  def bloodthirst(self, boss: Boss):
    if self.talents['bloodthirst'] and self.bloodthirst_cd <= 0 and self.has_rage(30, False):
      self.bloodthirst_cd = 6
      dmg = self.attack_power * 0.45
      dmg, crit = self.__attack(dmg, 30, boss)
      if dmg > 0:
        self.counters['bt'] += 1
        self.total_threat += self.__threat(dmg)
        self.on_hit_procs(boss, crit)
      #logging.info("  Bloodthirst for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      return True

    return False

  def revenge(self, boss: Boss):
    if self.revenge_active and self.revenge_cd <= 0 and self.has_rage(5, False):
      self.revenge_cd = 5
      dmg = random.randint(81, 99)
      dmg, crit = self.__attack(dmg, 5, boss)
      if dmg > 0:
        self.counters['rev'] += 1
        self.total_threat += self.__threat(dmg, 355)
        self.on_hit_procs(boss, crit)
      #logging.info("  Revenge for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      self.revenge_active = False
      return True

    return False

  def sunder_armor(self, boss: Boss):
    rage_cost = 15 - (self.talents['improved_sunder_armor'])
    if self.has_rage(rage_cost, False):
      dmg, crit = self.__attack(1000, rage_cost, boss)  # just to roll
      if dmg > 0:
        self.counters['sa'] += 1
        self.total_threat += self.__threat(0, 261)
        self.on_hit_procs(boss, False)
      #logging.info("  Sunder Armor %s. Total threat: %d" % ("hit" if dmg > 0 else "miss", self.total_threat))
      return True

    return False

  def heroic_strike(self, boss: Boss):
    rage_cost = 15 - (self.talents['improved_heroic_strike'])
    if self.has_rage(rage_cost, False):
      dmg = self.__weapon_damage(off_hand=False) + 157
      dmg, crit = self.__attack(dmg, rage_cost, boss)
      if dmg > 0:
        self.counters['hs'] += 1
        self.total_threat += self.__threat(dmg, 175)
        self.on_hit_procs(boss, crit)
      self.swing = self.__current_speed()
      #logging.info("  Heroic Strike for %d damage%s. Total threat: %d" % (dmg, " (critical)" if crit else "", self.total_threat))
      return True

    return False

  def __parry_haste(self):
    prev = self.swing
    remaining = self.swing/self.__current_speed()
    if remaining > 0.6:
      self.swing -= 0.4 * self.__current_speed()
    elif remaining > 0.2:
      self.swing = self.swing * 0.8
    #logging.info("  Parry haste! %.1f --> %.1f" % (prev, self.swing))

  def __weapon_damage(self, bonus_ap=0, off_hand=False):
    ap = self.attack_power + bonus_ap
    w = self.off_hand if off_hand else self.main_hand

    _min = int(w['min_damage'] + ap/14 * self.__current_speed(off_hand))
    _max = int(w['max_damage'] + ap/14 * self.__current_speed(off_hand))

    return random.randint(_min, _max)

  def __damage_mod(self, damage, off_hand=False):
    dmg = damage * (1.0 + self.talents['1h_spec'])

    if self.death_wish_buff > 0:
      dmg *= 1.2

    if self.enraged > 0:
      dmg *= (1.0 + self.talents['enrage'])

    if off_hand:
      dmg *= 0.5 * (1.0 + self.talents['dw_spec'])

    return int(dmg)

  def __rage(self, dmg, crit):
    d = dmg
    f = 7.0 if crit else 3.5
    s = self.__current_speed()
    c = 230.6

    r1 = (15*d)/(4*c) + (f*s)/2
    r2 = (15*d)/c
    return min(r1, r2)

  def __threat(self, dmg, bonus=0):
    return float(dmg + bonus) * 1.3 * (1.0 + self.talents['defiance'])

  def __attack(self, damage, rage_cost, boss: Boss, white=False, off_hand=False, sunder_armor=False):
    d = self.__damage_mod(damage, off_hand) * (1.0 - boss.dr)

    if rage_cost > 0 and self.__has_set("wrath", 5):
      if self.warriors_wrath:
        rage_cost -= 5
        self.warriors_wrath = False
        #logging.debug("  Warrior's Wrath consumed.")
      if random.random() <= 0.2:
        #logging.debug("  Warrior's Wrath!")
        self.warriors_wrath = True

    self.rage -= rage_cost

    w = self.off_hand if off_hand else self.main_hand
    t = w['type']
    weapon_skill = self.weapon_skills[t] - 300
    skill_diff = 15 - weapon_skill

    _miss = (0.05 + skill_diff * 0.001) if skill_diff <= 10 else (0.07 + (skill_diff - 10) * 0.004)
    if white and self.off_hand is not None and self.off_hand['type'] != "shield":
      _miss += 0.19  # ??
    _miss -= self.hit
    _miss = max(0.00, _miss)
    _dodge = 0.05 + skill_diff * 0.001
    _parry = 0.14
    _crit = self.crit - skill_diff * 0.004
    # TODO: Block
    
    _glancing_chance = 0
    _glancing_mod = 1.0
    if white:
      _glancing_chance = 0.1 + 15 * 0.02  # Only base weapon skill is counted here
      _glancing_mod = 1.0 - (0.3 - 0.03 * weapon_skill)
      _glancing_mod = min(0.95, _glancing_mod)
      #if not self.TRACED:
      #  print("<%s> Miss: %.2f%%, Dodge: %.2f%%, Parry: %.2f%%, Glance: %.2f%% (%.1f%%), Crit: %.2f%%" % 
      #    (self.weapon_name, _miss*100, _dodge*100, _parry*100, _glancing_chance*100, _glancing_mod*100, _crit*100))
      #  self.TRACED = True

    is_crit = False
    roll = random.random()
    if roll <= _miss:
      d = 0
    elif roll <= (_miss + _dodge):
      d = 0
      self.rage += round(rage_cost * 0.77)  # refund
    elif roll <= (_miss + _dodge + _parry):
      d = 0
      self.rage += round(rage_cost * 0.77)  # refund
      boss.parry_haste()
    elif roll <= (_miss + _dodge + _parry + _glancing_chance):
      d *= _glancing_mod
    elif roll <= (_miss + _dodge + _parry + _glancing_chance + _crit):
      d *= (2.0 + (0 if white else self.talents['impale']))
      is_crit = True

    if d > 0 and self.talents['unbridled_wrath'] > 0:
      if random.random() <= self.talents['unbridled_wrath']:
        #logging.info("  Unbridled Wrath proc.")
        self.__gain_rage(1)

    if not sunder_armor:
      self.damage_done += d

    return round(d), is_crit

  def defend(self, damage, is_magic=False):
    d = damage

    if is_magic:
      roll = random.random()
      if roll <= 0.04:
        #print("resist!!")
        d = 0
      #else:
      #  d = damage
    else:
      _miss = 0.05 + (315 - self.defense) * 0.0004
      _dodge = self.dodge
      _parry = self.parry
      if self.off_hand is not None and self.off_hand['type'] == "shield":
        _block = self.block + (0.75 if self.shield_block_stacks > 0 else 0)
      else:
        _block = 0
      _crush = 0.15
      _crit = 0.05 + (315 - self.defense) * 0.0004

      roll = random.random()
      if roll <= _miss:
        #logging.debug("  Boss swing MISSED")
        d = 0
      elif roll <= (_miss + _dodge):
        #logging.debug("  Boss swing DODGED")
        self.revenge_active = True
        d = 0
      elif roll <= (_miss + _dodge + _parry):
        #logging.debug("  Boss swing PARRIED")
        self.revenge_active = True
        d = 0
        self.__parry_haste()
      elif roll <= (_miss + _dodge + _block):
        #logging.debug("  Boss swing BLOCKED")
        self.revenge_active = True
        d -= self.block_value
        #logging.info("  Shield spec rage.")
        self.__gain_rage(1)  # shield spec
        self.shield_block_stacks -= 1
      elif roll <= (_miss + _dodge + _block + _crush):
        #logging.debug("  Boss swing CRUSHED")
        d *= 1.5
      elif roll <= (_miss + _dodge + _block + _crush + _crit):
        #logging.debug("  Boss swing CRIT")
        d *= 2
        self.enrage()

      if self.death_wish_buff > 0:
        a = float(self.armor / 1.2)
        dr = a / (a + 400 + 85 * (63 + 4.5 * (63 - 59)))
      else:
        dr = float(self.armor) / float(self.armor + 400 + 85 * (63 + 4.5 * (63 - 59)))

      d *= (1.0 - dr)

    if d > 0:
      d = round(d)
      #logging.info("  Took %d damage." % d)
      self.damage_taken += d
      self.__gain_rage((5/2) * (d/230.6), was_hit=True)

  def __gain_rage(self, rage, was_hit = False):
    self.rage += rage
    if was_hit and self.__has_set("might", 5) and random.random() <= 0.2:
      self.rage += 1
      #logging.info("  1 extra rage from Might set-bonus.")
    if self.rage > 100:
      self.rage = 100

    #logging.info("  RAGE! Gained: %d, Current: %d" % (rage, self.rage))

  def __has_set(self, name, n):
    return (name in self.sets) and self.sets[name] >= n

  def has_rage(self, r, br=True):
    _rage = self.rage
    if br and self.bloodrage_cd <= 1.5:
      _rage += 10 + self.talents['improved_bloodrage']
    if self.warriors_wrath:
      _rage += 5
    return _rage >= r


def simulate(player: Player, boss: Boss, duration, details=False):
  _t = 0
  _gcd = 0
  delay = 0.1
  _hs_queued = False

  #__start = time.time_ns()

  __wh = player.white_hit
  __hs = player.heroic_strike
  __sb = player.shield_block
  __br = player.bloodrage
  __dw = player.death_wish
  __ss = player.shield_slam
  __bt = player.bloodthirst
  __sa = player.sunder_armor
  __rev = player.revenge
  __hasrage = player.has_rage
  __tick = player.tick
  __b_tick = boss.tick
  __b_atk = boss.attack

  while _t < duration:
    ## 1. Are we swinging?
    if player.swing <= 0:
      ##logging.info("  [%.1f] SWING!" % _t)
      if _hs_queued:
        _hs_queued = False
        if not __hs(boss):  # ie. not enough rage
          __wh(boss)
      else:
        __wh(boss)

    ## 1.5. Off-hand swing?
    if player.oh_swing <= 0:
      ##logging.info("  [%.1f] OH SWING!" % _t)
      __wh(boss, off_hand=True)

    ## 2. Is boss swinging?
    __b_atk(player)

    ## 3. Shield Block if possible
    __sb()

    ## 4. Use ability if not on GCD
    __sb_cd = player.shield_block_cd
    __ss_cd = player.shield_slam_cd

    if _gcd <= 0:
      if __br():
        _gcd = 1.5
      elif __dw():
        _gcd = 1.5
      elif __sb_cd > 0:  # ...and we're not saving rage for Shield Block
        if __ss(boss):  # Use shield slam if possible
          _gcd = 1.5
        elif __hasrage(45) and __bt(boss):  # Use bloodthirst if possible, basically a rage dump if we happen to have too much rage here
          _gcd = 1.5
        elif player.revenge_active and player.revenge_cd <= 0 and __hasrage(5):  ## Revenge is possible...
          use = False
          if __sb_cd >= 3:
            use = __ss_cd >= 3 or __hasrage(25)
          if not use and __ss_cd >= 3:
            use = __sb_cd >= 3 or __hasrage(15)
          if not use and __hasrage(35):
            use = True
          if use:
            __rev(boss)  # Use revenge
            _gcd = 1.5
        else:  ## Dump rage on Sunder armor?
          dump = False
          if __sb_cd >= 3:
            dump = __ss_cd >= 3 or __hasrage(45)
          if not dump and __ss_cd >= 3:
            dump = __sb_cd >= 3 or __hasrage(35)
          if not dump and __hasrage(55):
            dump = True
          if dump and __ss_cd > 1.5 and player.revenge_cd > 1.5:
            __sa(boss)
            _gcd = 1.5

    ## 5. Dump rage on Heroic Strike?
    _hs_queued = False
    if __sb_cd >= 3:
      _hs_queued = __ss_cd >= 3 or __hasrage(55)
    if not _hs_queued and __ss_cd >= 3:
      _hs_queued = __sb_cd >= 3 or __hasrage(45)
    if not _hs_queued and __hasrage(65):
      _hs_queued = True


    __tick(delay)
    __b_tick(delay)
    _gcd -= delay
    _t += delay

  #tt = (time.time_ns() - __start) / (10 ** 9)
  #print("%1.6f fight done" % tt)

  if details:
    return (float(player.total_threat)/duration, 
      float(player.damage_taken)/duration, 
      float(player.damage_done)/duration, 
      player.counters['wh'],
      player.counters['wf'],
      player.counters['rev'],
      player.counters['hs'],
      player.counters['ss'],
      player.counters['sa'],
      player.counters['sb'],
      player.counters['bt'])
  else:
    return (float(player.total_threat)/duration, 
      float(player.damage_taken)/duration)


def main():
  input_file = sys.argv[1] if len(sys.argv) > 1 else "input.json"
  csv_file = sys.argv[2] if len(sys.argv) > 2 else None
  start = int(sys.argv[3]) if len(sys.argv) > 3 else 1

  settings = {}
  with open(input_file) as f:
    settings = json.load(f)

  fight_duration = int(settings['fight_duration'])
  n_fights = int(settings['fights'])
  items = settings['items']
  combinations = list(itertools.product(*items))
  details = bool(settings['details'])
  hit_cap = int(settings['player']['hit_cap'])

  tot = n_fights*len(combinations)

  # #logging.basicConfig(filename='combat.log', filemode='w', level=(#logging.DEBUG if tot == 1 else #logging.WARNING))

  print("Fights: %d, duration: %d seconds, gear combinations: %d, total iterations: %d" % (n_fights, fight_duration, len(combinations), tot))
  print("Talent spec: %s" % settings['player']['talents']['name'])
  print("Boss: %s (~%.0f DPS)%s" % (settings['boss']['name'] if not settings['player']['infinite_rage'] else "Vaelastrasz", 
    (settings['boss']['damage'] / settings['boss']['speed']),
    ", Off-tank" if settings['player']['off_tank'] else ""))

  _equipped = None
  mean_results = {}
  counter = 0
  _set_time = time.time()

  for _set in combinations:
    counter += 1
    if counter > 1 and counter < start:  # Always run the first one (currently equipped)
      continue
    if counter % 100 == 0:
      print(counter, end='', flush=True)

    names = [i['name'] for i in _set]
    name = " + ".join(names)

    if not name:
      name = settings['player']['main_hand']['name'] + " + " + settings['player']['off_hand']['name']

    if _equipped is None:
      _equipped = _set
      print("Currently equipped: " + name)

    results = []

    if hit_cap >= 1:
      #print("Check hit cap...")
      p = Player(settings['player'], _set, _equipped, wf=True, debug=False)
      _hit = int(p.hit*100)
      if _hit != hit_cap and p.off_hand['type'] == "shield":
        #print("SKIP %s! (hit %d (%.5f) != %d)" % (name, _hit, p.hit, hit_cap))
        print("!", end='', flush=True)
        continue

    print(".", end='', flush=True)

    #_start = time.time_ns()

    with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
      futures = []

      for n in range(n_fights):
        boss = Boss(settings['boss'])

        player = Player(settings['player'], _set, _equipped, wf=True)
        futures.append(executor.submit(simulate, player, boss, fight_duration, details))

      for future in concurrent.futures.as_completed(futures):
        tps = future.result()
        results.append(tps)

    mean_results[name] = tuple(map(mean, zip(*results)))
    #_stop = time.time_ns()

    if csv_file:
      with open(csv_file, 'a') as f:
        #f.write("%d,%.6f,%.2f,%.1f,%s\n" % (counter, (_stop - _start) / (10 ** 9) , mean_results[name][0], mean_results[name][1], name))
        f.write("%d,%.2f,%.1f,%s\n" % (counter, mean_results[name][0], mean_results[name][1], ",".join(names)))

    tmp = _set_time
    _set_time = time.time()
    tmp = _set_time - tmp
    #print("Time:", tmp)
    #print("Time remaining:", (tmp*(len(combinations)-counter-1)))


  print("\n\nResults (sorted by TPS):\n...");
  print("[  TPS,  DTPS] Windfury: true\n=======================================================")
  max = 100
  for k in {k: v for k ,v in sorted(mean_results.items(), key=lambda item: item[1][0], reverse=True)}:  # TODO remove unnecessary packing
    if details:
      print("[%5.1f, %5.1f] %s (%.1f white hits, %.1f revenge, %.1f HS, %.1f SS, %.1f SA, %.1f SB, %.1f BT)" % 
        (mean_results[k][0], mean_results[k][1], k, mean_results[k][3], mean_results[k][5], mean_results[k][6], mean_results[k][7], mean_results[k][8], mean_results[k][9], mean_results[k][10]))
    else:
      print("[%5.1f, %5.1f] %s" % (mean_results[k][0], mean_results[k][1], k))

    max -= 1
    if max <= 0:
      break

  #print("\n\nResults (sorted by DTPS):\n...");
  #print("[  TPS,  DTPS] Windfury: true\n=======================================================")
  #max = 10
  #for k in {k: v for k ,v in sorted(mean_results.items(), key=lambda item: item[1][1], reverse=False)}:  # TODO remove unnecessary packing
  #  if settings['details']:
  #    print("[%5.1f, %5.1f] %s (%.1f white hits, %.1f revenge, %.1f HS, %.1f SS, %.1f SA, %.1f SB, %.1f BT)" % 
  #      (mean_results[k][0], mean_results[k][1], k, mean_results[k][3], mean_results[k][5], mean_results[k][6], mean_results[k][7], mean_results[k][8], mean_results[k][9], mean_results[k][10]))
  #  else:
  #    print("[%5.1f, %5.1f] %s" % (mean_results[k][0], mean_results[k][1], k))
#
  #  max -= 1
  #  if max <= 0:
  #    break

if __name__ == '__main__':
    main()