#!/usr/bin/env python3

import random
import pprint
import copy

class Hunter:
    def __init__(self, brain, verbose=False):
        self.aimed = brain[0] + [0, True, self.aimed_shot]
        self.raptor = brain[1] + [0, False, self.raptor_strike]
        self.multi = brain[2] + [0, True, self.multi_shot]
        self.rapid = brain[3] + [0, False, self.rapid_fire]
        self.zerk = brain[4] + [0, False, self.berserking]
        self.charm = brain[5] + [0, False, self.charm_trinket]
        self.devil = brain[6] + [0, False, self.devil_trinket]
        self.swap_both_trinket_same_time = brain[7]

        self.abilities = [self.aimed, self.raptor, self.multi, self.rapid, self.zerk, self.charm, self.devil]
        self.abilities = sorted(self.abilities, key=lambda x: x[0])

        # state
        self.t = -1
        self.gcd = 1
        self.as_t = 1
        self.buffs = []
        self.rapid_active = False
        self.zerk_active = False
        self.dmg = 0
        self.feign_cd = 0
        self.used_charm = False
        self.used_devil = False
        self.trinket_1_swap = False
        self.trinket_2_swap = False
        self.opener_done_t = 0

        self.ranged_miss = 0
        self.ranged_crit = 0.269 - 15 * 0.004
        self.ranged_bonus_ap = 0
        self.melee_miss = 0.02
        self.melee_dodge = 0.05 + 15 * 0.001
        self.melee_crit = 0.219 - 15 * 0.004
        self.melee_bonus_ap = 0

        self.verbose = verbose

    def log(self, str):
        if self.verbose:
            print(str)

    def auto_shot(self, no_damage=False):
        speed = 3.4
        if self.rapid_active:
            speed /= 1.4
        if self.zerk_active:
            speed /= 1.1
        self.as_t = int(speed * 10)
        if not no_damage:
            dmg = self.ranged_damage(753.2, speed)
            self.dmg += dmg
            self.log("[%.1f] Auto - %d damage" % (self.t/10.0, dmg))

    def aimed_shot(self):
        self.log("[%.1f] Start cast: Aimed Shot" % (self.t/10.0))
        cast = 30
        if self.rapid_active:
            cast /= 1.4
        if self.zerk_active:
            cast /= 1.1
        dmg = self.ranged_damage(1270.6, 2.8)
        self.dmg += dmg
        self.aimed[3] = 60 + int(cast)
        self.tick(int(cast), busy=True)
        self.log("[%.1f] Aimed Shot - %d damage" % (self.t/10.0, dmg))

    def multi_shot(self):
        self.log("[%.1f] Start cast: Multi Shot" % (self.t/10.0))
        cast = 5
        if self.rapid_active:
            cast /= 1.4
        if self.zerk_active:
            cast /= 1.1
        dmg = self.ranged_damage(921.3, 2.8)
        self.dmg += dmg
        self.multi[3] = 100 + int(cast)
        self.tick(int(cast), busy=True)
        self.log("[%.1f] Multi Shot - %d damage" % (self.t/10.0, dmg))

    def ranged_damage(self, base_dmg, speed):
        dmg = base_dmg + self.ranged_bonus_ap/14*speed
        miss = max(0, self.ranged_miss)
        return int(dmg*(1-miss)*(1+self.ranged_crit*1.3))

    def raptor_strike(self):
        speed = 3.2 if self.zerk_active else 3.5
        dmg = 1532.7 + self.melee_bonus_ap/14*speed
        miss = max(0, self.melee_miss) + self.melee_dodge
        expected_wf_dmg = 0.275 * ((dmg-140)+(315/14)*speed)*(1+self.melee_crit)
        dmg = dmg*(1-miss)*(1+self.melee_crit)+expected_wf_dmg

        self.dmg += dmg
        self.log("[%.1f] Run forward" % (self.t/10.0))
        self.tick(15, busy=True)  # todo: double check
        self.raptor[3] = 60
        self.log("[%.1f] Melee weave - %d damage" % (self.t/10.0, dmg))
        self.tick(15, busy=True)
        self.log("[%.1f] Back in sweet spot" % (self.t/10.0))
        self.as_t = max(self.as_t, 5)  # we moved so min 0.5s auto shot timer

    def rapid_fire(self):
        self.buffs.append([150, "rapid"])
        self.rapid[3] = 3000
        self.rapid_active = True
        self.log("[%.1f] Rapid Fire" % (self.t/10.0))

    def berserking(self):
        self.buffs.append([100, "zerk"])
        self.zerk[3] = 1800
        self.zerk_active = True
        self.log("[%.1f] Berserking" % (self.t/10.0))

    def charm_trinket(self):
        self.used_charm = True
        self.charm[3] = 1800
        self.aimed[3] = 0
        self.multi[3] = 0
        self.devil[3] = max(self.devil[3], 200)
        self.log("[%.1f] Charm" % (self.t/10.0))

    def devil_trinket(self):
        self.buffs.append([200, "devil"])
        self.devil[3] = 1200
        self.charm[3] = max(self.charm[3], 200)
        self.ranged_miss -= 0.02
        self.melee_miss -= 0.02
        self.ranged_bonus_ap += 150
        self.melee_bonus_ap += 150
        self.log("[%.1f] Devilsaur's Eye" % (self.t/10.0))

    def tick(self, d, busy=False):
        for i in range(d):  # tick one by one to handle buffs
            self.t += 1
            self.gcd -= 1
            self.as_t -= 1
            self.feign_cd -= 1

            if self.as_t <= 0 and not busy:
                self.auto_shot()

            for a in self.abilities:
                a[3] -= 1

            for b in self.buffs:
                b[0] -= 1
                if b[0] <= 0:
                    self.log("[%.1f] %s fades" % (self.t/10.0, b[1]))
                    if b[1] == "devil":
                        self.ranged_miss += 0.02
                        self.melee_miss += 0.02
                        self.ranged_bonus_ap -= 150
                        self.melee_bonus_ap -= 150
                        self.used_devil = True
                    elif b[1] == "rapid":
                        self.rapid_active = False
                    elif b[1] == "zerk":
                        self.zerk_active = False

            self.buffs = [b for b in self.buffs if b[0] > 0]

    def trinket_swap(self):
        if self.gcd > 0:
            self.tick(self.gcd)  # wait for gcd first
        self.feign_cd = 300
        self.auto_shot(no_damage=True)  # todo: hmm, auto shot delay?
        self.log("[%.1f] Feign Death" % (self.t/10.0))

        u = 0
        u += 1 if self.used_charm else 0
        u += 1 if self.used_devil else 0

        if not self.trinket_1_swap:
            self.trinket_1_swap = True
            self.ranged_crit += 0.02
            self.melee_crit += 0.02
            if self.used_devil:
                self.log("[%.1f] Swapped Devilsaur's Eye to Blackhand's Breadth" % (self.t/10.0))
            elif self.used_charm:
                self.log("[%.1f] Swapped Charm to Blackhand's Breadth" % (self.t/10.0))
        if self.used_charm and self.used_devil and not self.trinket_2_swap:
            self.trinket_2_swap = True
            self.ranged_bonus_ap += 48
            self.log("[%.1f] Swapped other trinket for Royal Seal of Eldre'Thalas" % (self.t/10.0))

        self.gcd = 15

    def doit(self):
        while True:
            self.tick(1)
            lowest_cd = 99999

            for a in self.abilities:
                if a[1] < lowest_cd and a[2] <= self.as_t and a[3] <= 0 and (not a[4] or self.gcd <= 0):
                    if a[4]:
                        self.gcd = 15
                    a[5]()
                elif a[3] > 0 and a[3] < lowest_cd:
                    lowest_cd = a[3]

            if (not self.trinket_1_swap or not self.trinket_2_swap) and self.feign_cd <= 0:
                if self.swap_both_trinket_same_time and self.used_charm and self.used_devil:
                    self.trinket_swap()
                elif not self.swap_both_trinket_same_time:
                    u = 0
                    u += 1 if self.used_charm else 0
                    u += 1 if self.used_devil else 0
                    s = 0
                    s += 1 if self.trinket_1_swap else 0
                    s += 1 if self.trinket_2_swap else 0
                    if s < u:
                        self.trinket_swap()

            if self.rapid[3] > 0 and self.zerk[3] > 0 and self.trinket_1_swap and self.trinket_2_swap and not self.buffs and self.opener_done_t == 0:
                self.log("[%.1f] Opener finished! Waiting 5 more seconds..." % (self.t/10.0))
                self.opener_done_t = self.t

            if self.opener_done_t > 0 and (self.t - self.opener_done_t) >= 50:  # Include additional 5s after opener
                dps = float(self.dmg)/(self.t/10);
                self.log("[%.1f] DONE! - %.1f DPS" % (self.t/10.0, dps))
                return dps

            if self.t >= 900:  # sanity
                dps = float(self.dmg)/(self.t/10);
                self.log("[%.1f] DONE! - %.1f DPS" % (self.t/10.0, dps))
                return dps


def make_generation(mom=None, dad=None, size=30):
    generation = [mom, dad] if mom and dad else []
    pp = pprint.PrettyPrinter(indent=2)

    for i in range(size):
        brain = []
        if not mom or not dad:
            for x in range(7):
                brain.append([random.randint(0, 200), random.randint(0, 200), random.randint(0, 200)])
            brain.append(True if random.randint(0, 1) == 1 else False)
        else:
            for x in range(7):
                part = []
                for y in range(3):
                    if random.randint(1, 100) < 16:  # mutation
                        v = random.randint(0, 200)
                    else:
                        _min = min(mom[x][y], dad[x][y])
                        _max = max(mom[x][y], dad[x][y])
                        v = random.randint(_min, _max)
                    part.append(v)
                brain.append(part)
            if mom[7] and dad[7]:
                brain.append(False if random.randint(0, 9) == 0 else True)
            elif mom[7] or dad[7]:
                brain.append(False if random.randint(0, 1) == 0 else True)
            else:
                brain.append(True if random.randint(0, 9) == 0 else False)

        generation.append(brain)

    return generation


def run_batch(name, start_gen=None, verbose=False):
    mom = None
    dad = None
    top_dps = 0
    top_brain = None
    for n in range(50000 if start_gen else 1000):
        gen = start_gen if n == 0 and start_gen else make_generation(mom, dad)
        results = []
        for brain in gen:
            h = Hunter(brain)
            dps = h.doit()
            results.append([dps, brain])

        results = sorted(results, key=lambda x: x[0])
        results.reverse()
        if verbose:
            print("GEN %d BEST: %.1f DPS, BRAIN: %s" % (n, results[0][0], results[0][1]))
        mom = results[0][1]
        dad = results[1][1]
        if results[0][0] > top_dps:
            top_dps = results[0][0]
            top_brain = mom
        if not verbose:
            if n % 100 == 0:
                print(".", end='', flush=True)

    print("\n --- BATCH %s BEST: %.1f DPS, BRAIN: %s" % (name, top_dps, top_brain))
    return top_brain


def main():
    super_generation = [
        [[64, 91, 14], [177, 29, 12], [112, 29, 1], [84, 36, 24], [33, 70, 26], [146, 12, 14], [35, 100, 17], False],
        [[107, 36, 10], [125, 12, 16], [84, 72, 24], [51, 121, 23], [151, 79, 3], [178, 4, 21], [72, 128, 22], True]
    ]
    #for x in range(30):
    #    top_brain = run_batch(str(x))
    #    super_generation.append(top_brain)
    print("\nRUNNING SUPER BATCH")
    run_batch("BEST OF THE", super_generation, True)

    #brain = [[107, 36, 10], [125, 12, 16], [84, 72, 24], [51, 121, 23], [151, 79, 3], [178, 4, 21], [72, 128, 22], True]
    #h = Hunter(brain, verbose=True)
    #h.doit()


if __name__ == "__main__":
    # execute only if run as a script
    main()

