#!/usr/bin/env python3

import random

def __weapon_damage(min_dmg, max_dmg, speed, crit, dr, bonus_ap=0):
  _min = int(min_dmg + bonus_ap/14 * speed)
  _max = int(max_dmg + bonus_ap/14 * speed)
  was_crit = False
  damage = random.randint(_min, _max)
  if random.random() <= crit:
    was_crit = True
    damage *= 2
  damage = int(damage * (1.0 - dr))
  return damage, was_crit

def simulate(extra_ap = 0, extra_crit = 0, extra_spellcrit = 0, extra_spellpower = 0):
  enemy_armor = 5000
  enemy_dr = float(enemy_armor) / float(enemy_armor + 400 + 85 * (60 + 4.5 * (60 - 59)))

  speed = 3.8
  min_damage = 338 + extra_ap/14 * speed
  max_damage = 488 + extra_ap/14 * speed
  crit = 0.2025 + extra_crit
  spell_power = 0 + extra_spellpower
  spell_crit = 0.0465 + extra_spellcrit
  shock_cd = 5.6
  stormstrike_cd = 20

  _t = 0
  _damage = 0
  _shock_cd = 0
  _stormstrike_cd = 0
  _gcd = 0
  _swing = 0
  _flurry = 0
  _ss_buff = 0
  _ed = 0

  TIME = 2400000

  while _t <= TIME:
    s = (speed * 0.7) if _flurry > 0 else speed
    c = (crit + 0.09) if _ed > 0 else crit 

    if _swing <= 0:
      _flurry -= 1
      _swing = s
      dmg, was_crit = __weapon_damage(min_damage, max_damage, s, c, enemy_dr)
      #print("%.1f :: White hit :: %d damage%s" % (_t, dmg, " (critical)" if was_crit else ""))
      _damage += dmg
      if was_crit:
        _flurry = 3

      if random.random() <= 0.2:
        dmg1, was_crit = __weapon_damage(min_damage, max_damage, s, c, enemy_dr, 333 * 1.4)
        if was_crit:
          _flurry = 3
        dmg2, was_crit = __weapon_damage(min_damage, max_damage, s, c, enemy_dr, 333 * 1.4)
        if was_crit:
          _flurry = 3
        #print("%.1f :: Windfury proc :: %d + %d damage" % (_t, dmg1, dmg2))
        _damage += dmg1
        _damage += dmg2

    if _gcd <= 0:
      if _stormstrike_cd <= 0:
        _stormstrike_cd = stormstrike_cd
        _gcd = 1.5
        dmg, was_crit = __weapon_damage(min_damage, max_damage, s, c, enemy_dr)
        #print("%.1f :: Stormstrike :: %d damage%s" % (_t, dmg, " (critical)" if was_crit else ""))
        _damage += dmg
        if was_crit:
          _flurry = 3
        _ss_buff = 2
      elif _shock_cd <= 0:
        _shock_cd = shock_cd
        _gcd = 1.5
        dmg = random.randint(517, 545) + spell_power * 0.4071
        dmg *= 1.05
        was_crit = False
        if random.random() <= spell_crit:
          dmg *= 1.5
          was_crit = True
          _ed = 10
        if _ss_buff > 0:
          dmg *= 1.2
          _ss_buff -= 1
        #print("%.1f :: Earth Shock :: %d damage%s" % (_t, dmg, " (critical)" if was_crit else ""))
        _damage += dmg

    _t += 0.1
    _shock_cd -= 0.1
    _stormstrike_cd -= 0.1
    _swing -= 0.1
    _gcd -= 0.1
    _ed -= 0.1

  print("\nCOMPLETE! Total damage = %d (%.3f DPS)" % (_damage, float(_damage)/TIME))
  

def main():
  simulate()
  simulate(extra_ap=100)
  simulate(extra_crit=0.05)
  simulate(extra_spellcrit=0.05)
  simulate(extra_spellpower=100)

if __name__ == '__main__':
    main()